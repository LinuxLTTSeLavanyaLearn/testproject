#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define NUMBER_OF_THREADS 3
#define NUM_OF_ELEMENTS_THREAD 4  

int arr[12]={1,3,5,7,9,2,4,6,8,10,12,14};
int min_max[NUM_OF_ELEMENTS_THREAD]= {0};
int part = 0;

int maxval=0,minval=0;


void * min_max_arr(void *arg)
{
  int i,partOfThread = part++;
 for(i=partOfThread*(sizeof(arr)/NUM_OF_ELEMENTS_THREAD); i<(partOfThread+1)*(sizeof(arr)/NUM_OF_ELEMENTS_THREAD); i++)
if(arr[i]> maxval)
    maxval = arr[i];
else
minval = arr[i];

}

int main()
{
    int i,total=0;
    pthread_t tid[NUMBER_OF_THREADS];

    for (i = 0; i < NUMBER_OF_THREADS; i++)
        pthread_create(&tid, NULL, min_max_arr, (void *)NULL);

    for(i = 0; i< NUMBER_OF_THREADS; i++)
        pthread_join(tid[i],NULL);

//for(i=0; i<NUMBER_OF_THREADS; i++)
//        total+=sum[i];

   printf("maxval is %d\n,minval is %d\n",maxval,minval);

   return 0;
}

