#include <time.h>
#include<stdio.h>

void square()
{
int i, arr[10]={1,2,3,4,5,6,7,8,9,10};
for(i=0;i<=9;i++)
printf("%d * %d = %d\n",arr[i], arr[i],arr[i] *arr[i]);

}


void main()
{
struct timespec start, finish; 
long int sec,nsec;
    clock_gettime(CLOCK_REALTIME, &start); 

    square();

    clock_gettime(CLOCK_REALTIME, &finish); 

    sec = finish.tv_sec - start.tv_sec; 
    nsec = finish.tv_nsec - start.tv_nsec; 
    
    printf("seconds: %ld\n", sec); 
    printf("nanoseconds: %ld\n", nsec); 
    printf("total seconds: %e\n", (double)sec + (double)nsec/(double)1000000000);

}


