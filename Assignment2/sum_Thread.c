#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <pthread.h> 

#define NUMBER_OF_THREADS 3
#define NUM_OF_ELEMENTS_THREAD 4  

int arr[12]={1,3,5,7,9,2,4,6,8,10,12,14};
int sum[NUM_OF_ELEMENTS_THREAD]= {0};
int part = 0;

  

void * sum_arr(void *arg)
{
  int i,partOfThread = part++;
  for(i=partOfThread*(sizeof(arr)/NUM_OF_ELEMENTS_OF_THREADS; i<(partOfThread+1)*(sizeof(arr)/NUM_OF_ELEMENTS_THREADS);i++)
    sum[partOfThread] += arr[i];
}

int main() 
{ 
    int i,total=0; 
    pthread_t tid[NUMBER_OF_THREADS]; 
   
    for (i = 0; i < NUMBER_OF_THREADS; i++) 
        pthread_create(&tid, NULL, sum_arr, (void *)NULL);

    for(i = 0; i< NUMBER_OF_THREADS; i++)
	pthread_join(tid[i],NULL);
   
    for(i=0; i<NUMBER_OF_THREADS; i++)
	total+=sum[i];

   printf("total sum is %d\n",total); 
  
   return 0; 
} 

