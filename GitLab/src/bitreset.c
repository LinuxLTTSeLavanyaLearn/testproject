#include "bitmask.h"
#include <stdio.h>

int bitReset(int num, int pos) 
{ 
    return (num & (~(1 << (pos - 1)))); 
} 
