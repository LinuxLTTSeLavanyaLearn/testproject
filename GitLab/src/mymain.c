#include "mystring.h"
#include "myutils.h"
#include "bitmask.h"
#include<stdio.h>
#include<stdarg.h>
#include<stdlib.h>

int main()
{

char s1[100],s2[100];
int num,pos;
 printf(" Enter the First String:\n");
scanf("%s",s1);
printf("Enter the second string:\n");
scanf("%s",s2);

mystrcpy(s1);
mystrcmp(s1,s2);
mystrcat(s1,s2);
printf("length of the first string is %d\n",mystrlen(s1));

printf("Enter a number:\n");
scanf("%d",&num);
prime(num);
palindrome(num);
printf("sum of given numbers is %d\n",vsum(2,6,4));
printf("factorial of %d is %d\n",num, factorial(num));

printf("Enter a num and bit position to SET\n");
scanf("%d%d",&num,&pos);
printf("after bit set the number is %d\n",setBit(num,pos));

printf("Enter a num and bit position to RESET\n");
scanf("%d%d",&num,&pos);
printf("after bit Reset the number is %d\n",bitReset(num,pos));

printf("Enter a num and bit position to Flip\n");
scanf("%d%d",&num,&pos);
printf("after bit toggle the number is %d\n",bitFlip(num,pos));

printf("Enter a num and bit position to know set or not\n");
scanf("%d%d",&num,&pos);
isBitSet(num,pos);

}
