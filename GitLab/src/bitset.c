#include "bitmask.h"
#include<stdio.h>

int setBit(int num, int pos) 
{ 
    return (num | (1 << (pos - 1))); 
} 
