#include "bitmask.h"
#include<stdio.h>

int bitFlip(int num, int pos) 
{ 
    return (num ^ (1 << (pos - 1))); 
} 
