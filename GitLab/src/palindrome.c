#include "myutils.h"
#include <stdio.h>


void palindrome(int num){
      
	int number = num;
	int remainder, reverse;

    while (num != 0) {
        remainder = num % 10;
        reverse = reverse * 10 + remainder;
        num /= 10;
    }
    if (number == reverse)
    {
        printf("%d is not a palindrome\n", number);
    }    
    else {
        printf("%d is a palindrome\n", number);
    }
    
}
