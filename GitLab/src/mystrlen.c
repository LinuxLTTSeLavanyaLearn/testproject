#include "mystring.h"
#include <stdio.h>

int mystrlen(char *txt)
{
	int i=0,count=0;
	
	while(txt[i++]!='\0'){
		count+=1;
	}
	
	return count;
}
